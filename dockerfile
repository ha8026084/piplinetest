FROM node:20.13.1-alpine3.20 as builder

WORKDIR /app

COPY package.json .


RUN yarn install 

COPY . .

RUN yarn build

EXPOSE 80

CMD ["yarn", "start"]